#!/bin/bash

S1="hakkinnen.helloworldopen.com"
S2="senna.helloworldopen.com"
S3="webber.helloworldopen.com"

HOST=$S2
TRACK="germany"
OPTS="--key ${BOTKEY} --host ${HOST} --track ${TRACK} --password lesupersecretpasswort_tiba --carcount 2"

echo "Starting race on track \"${TRACK}\", server is ${HOST}"

./noobbot --name "Tiba Noob" $OPTS --create &> log1.log &
# Save PID
echo "Bot 1 started in background with PID ${!}"
./fixed_throttle --name "Tiba Fixed" $OPTS &> log2.log &
echo "Bot 2 started in background with PID ${!}"
