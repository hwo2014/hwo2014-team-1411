package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"tiba/comm"
	"tiba/game"
	"tiba/messages"
	"time"
)

var throttle float64 = 0.64

func dispatch_msg(ml *comm.MsgLoop, msg interface{}) (err error) {
	switch m := msg.(type) {
	case *messages.Error:
		err = errors.New(fmt.Sprint("Server-Errormessage: ", m.Data))
		return
	case *messages.CarPositions:
		game.SendThrottle(ml, throttle)
	case *messages.GenericMsg:
		log.Printf("Failed to handle message of type %v\n", m.MsgType)
	default:
		log.Printf("Failed to handle message of type %T\n", m)
		game.SendPing(ml)
		return
	}
	return
}

func bot_loop(ml *comm.MsgLoop) (err error) {
	log.Print("Starting loop")
	for m := range ml.ServerOut {
		input, err := game.ParseMsg(m)
		if err != nil {
			log_and_exit(err)
		}
		err = dispatch_msg(ml, input)
		if err != nil {
			log_and_exit(err)
		}
	}

	log.Println("Message loop closed channel")
	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {
	tmpThrottle := flag.Float64("throttle", 0.5, "Fixed throttle to use during the race")

	host, port, botId, raceData, gameMode := game.StandardFlags()

	throttle = *tmpThrottle

	// Verify that we have key

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, botId.Name, botId.Key)

	conn, err := comm.Connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	// Start message loop
	fmt.Println("Starting message loop")
	ml, err := comm.StartMessageLoop(conn, time.Second*time.Duration(30))
	if err != nil {
		log.Println("Failed to start message loop")
		log_and_exit(err)
	}

	err = game.StartGame(ml, botId, raceData, gameMode)
	if err != nil {
		log.Print("Failed to start game")
		log_and_exit(err)
	}

	fmt.Println("Sent game start message")

	err = bot_loop(ml)
	if err != nil {
		log.Print("Failure in boot loop")
		log_and_exit(err)
	}
}
