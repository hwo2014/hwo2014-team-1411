package messages

import (
	"errors"
	"fmt"
)

type ParseHelper interface {
	SetMsgType(msgType string)
	DataPointer() interface{}
}

func CreateMsg(msgType string) (msg ParseHelper) {
	switch msgType {
	case "error":
		msg = new(Error)
	case "join":
		msg = new(Join)
	case "joinRace":
		msg = new(JoinRace)
	case "createRace":
		msg = new(CreateRace)
	case "yourCar":
		msg = new(YourCar)
	case "carPositions":
		msg = new(CarPositions)
	case "gameInit":
		msg = new(GameInit)
	case "gameStart":
		msg = new(GameStart)
	case "crash":
		msg = new(Crash)
	case "spawn":
		msg = new(Spawn)
	default:
		fmt.Println("Generating generic message with type", msgType)
		msg = new(GenericMsg)
	}

	msg.SetMsgType(msgType)
	return
}

type GenericMsg struct {
	msgTyp
	msgData
}

type msgData struct {
	Data interface{} `json:"data"`
}

func (m *msgData) DataPointer() interface{} {
	return &m.Data
}

type Error struct {
	msgTyp
	Data string
}

func (e *Error) DataPointer() interface{} {
	return &e.Data
}

type CreateRace createJoinRace
type JoinRace createJoinRace

type createJoinRace struct {
	msgTyp
	Data createJoinRaceData `json:"data"`
}

type createJoinRaceData struct {
	BotId BotId `json:"botId"`
	RaceData
}

func (cj *JoinRace) DataPointer() interface{} {
	return &cj.Data
}

func (cj *CreateRace) DataPointer() interface{} {
	return &cj.Data
}

type msgTyp struct {
	MsgType string `json:"msgType"`
}

func (mt *msgTyp) SetMsgType(msgType string) {
	mt.MsgType = msgType
}

type ingameMsgData struct {
	msgTyp
	GameTick int `json:"gameTick,omitempty"`
}

type Join struct {
	msgTyp
	Data BotId `json:"data"`
}

func (j *Join) DataPointer() interface{} {
	return &j.Data
}

type ping struct {
	ingameMsgData
	Data *interface{} `json:"data,omitempty"`
}

func CreatePing(tick int) ping {
	p := ping{}
	p.MsgType = "ping"
	p.GameTick = tick

	return p
}

type throttle struct {
	ingameMsgData
	Data float64 `json:"data"`
}

func CreateThrottle(throt float64, tick int) throttle {
	t := throttle{}
	t.MsgType = "throttle"
	t.Data = throt
	t.GameTick = tick

	return t
}

type YourCar struct {
	msgTyp
	Data CarId `json:"data"`
}

func (yc *YourCar) DataPointer() interface{} {
	return &yc.Data
}

type CarPositions struct {
	ingameMsgData
	Data []CarPosition `json:"data"`
}

func (cp *CarPositions) DataPointer() interface{} {
	return &cp.Data
}

type GameInit struct {
	ingameMsgData
	Data struct {
		Race Race `json:"race"`
	} `json:"data"`
}

func (gi *GameInit) DataPointer() interface{} {
	return &gi.Data
}

func CreateCreateRace(botId BotId, raceData RaceData) (msg CreateRace, err error) {
	if raceData.CarCount < 1 {
		err = errors.New("Car count has to be bigger than 0")
		return
	}

	msg = CreateRace{}
	msg.MsgType = "createRace"
	msg.Data.BotId = botId
	msg.Data.RaceData = raceData
	return
}

func CreateJoinRace(botId BotId, raceData RaceData) (msg JoinRace, err error) {
	if raceData.CarCount < 1 {
		err = errors.New("Car count has to be bigger than 0")
		return
	}

	msg = JoinRace{}

	msg.MsgType = "joinRace"
	msg.Data.BotId = botId
	msg.Data.RaceData = raceData
	return
}

func CreateJoin(botId BotId) (msg Join, err error) {
	msg = Join{}
	msg.MsgType = "join"
	msg.Data = botId
	return
}

type GameStart GenericMsg

type Crash struct {
	ingameMsgData
	Data CarId
}

func (c *Crash) DataPointer() interface{} {
	return &c.Data
}

type Spawn struct {
	ingameMsgData
	Data CarId
}

func (s *Spawn) DataPointer() interface{} {
	return &s.Data
}

type CarId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type CarPosition struct {
	Id            CarId
	Angle         float32
	PiecePosition piecePosition
	Lap           int
}

type piecePosition struct {
	PieceIndex      int
	InPieceDistance float32
	Lane            LaneInfo
}

type LaneInfo struct {
	StartLaneIndex int
	EndLaneIndex   int
}

type BotId struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

type RaceData struct {
	TrackName string `json:"trackName,omitempty"`
	CarCount  int    `json:"carCount"`
	Password  string `json:"password,omitempty"`
}

type Race struct {
	Track       TrackDesc   `json:"track"`
	Cars        []Car       `json:"cars"`
	RaceSession interface{} `json:"raceSession"`
}

type TrackDesc struct {
	Id     string
	Name   string
	Pieces []Piece
	Lanes  []LaneDesc
	//Visualization, ignore StartingPoint interface{}
}

type Piece struct {
	Length float32
	Switch bool
	Radius float32
	Angle  float64
}

func (p *Piece) IsStraight() bool {
	return p.Length > 0
}

type LaneDesc struct {
	DistanceFromCenter float32
	Index              int
}

type Car struct {
	Id         CarId
	Dimensions CarDim
}

type CarDim struct {
	Length            float32
	Width             float32
	GuideFlagPosition float32
}
