package comm

import (
	"bufio"
	"encoding/json"
	"log"
	"net"
	"time"
)

type MsgLoop struct {
	writer        *bufio.Writer
	reader        *bufio.Reader
	ServerOut     <-chan []byte
	ServerTimeout time.Duration
	ServerIn      chan<- []byte

	stop chan int
}

func StartMessageLoop(conn net.Conn, timeout time.Duration) (loop *MsgLoop, err error) {
	loop = &MsgLoop{reader: bufio.NewReader(conn), writer: bufio.NewWriter(conn), ServerTimeout: timeout}

	servOut := make(chan []byte, 5)
	loop.ServerOut = servOut
	loop.stop = make(chan int)

	err = loop.startReceive(servOut, loop.stop)
	if err != nil {
		return
	}

	botIn := make(chan []byte, 5)
	loop.ServerIn = botIn
	err = loop.startSend(botIn)
	return
}

func (ml *MsgLoop) startSend(in <-chan []byte) (err error) {
	//Send loop
	go func() {
		for {
			msg, ok := <-in
			if !ok {
				//log.Println("[SendLoop] Bot closed message channel, so we quit send loop as well.")
				return
			}

			//log.Println("[SendLoop] Sending message ", string(msg))
			_, err := ml.writer.Write(msg)
			if err != nil {
				log.Println("[SendLoop] Failure sending message: ", err)
				return
			}

			_, err = ml.writer.WriteString("\n")
			if err != nil {
				log.Println("[SendLoop] Failure sending message: ", err)
				return
			}

			err = ml.writer.Flush()
			if err != nil {
				log.Println("[SendLoop] Failure sending message: ", err)
				return
			}
			//log.Println("[SendLoop] Message sent")
		}

	}()

	return
}

func (ml *MsgLoop) startReceive(out chan<- []byte, stop <-chan int) (err error) {
	receiveBuff := make(chan []byte)
	errChan := make(chan error, 1)

	go func() {
		for {
			line, err := ml.reader.ReadString('\n')

			if err != nil {
				errChan <- err
				close(receiveBuff)
				return
			}

			receiveBuff <- []byte(line)
		}
	}()

	//Receive loop
	go func() {
		for {
			//log.Println("[ReceiveLoop]", "Waiting for message")
			select {
			case msg, ok := <-receiveBuff:
				if !ok {
					log.Println("[ReceiveLoop] There was an error reading from the Server")
					serverErr := <-errChan
					log.Println("[ReceiveLoop] Error message:", serverErr)
					// Channel is closed, so we close our channel as well
					close(out)
					return
				}

				//log.Println("[ReceiveLoop] Received message: ", string(msg))

				out <- msg
			case <-time.After(ml.ServerTimeout):
				//Server did not send a message, close channel
				log.Println("[ReceiveLoop] Server Timeout, closing channel")
				close(out)
				return
			}
		}
	}()

	return
}

func (ml *MsgLoop) SendMsg(msg interface{}) (err error) {
	var payload []byte
	payload, err = json.Marshal(msg)
	if err != nil {
		log.Printf("Error creating json: %v\n", err.Error())
		return
	}

	ml.ServerIn <- payload
	return
}
