package comm

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"tiba/messages"
)

func Connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func TryParse(payload []byte) (msg interface{}, err error) {
	var tmpMsg unparsedMsg
	err = json.Unmarshal(payload, &tmpMsg)

	if err != nil {
		log.Println("Failed to parse json data: ", err)
		return
	}

	msgHelper := messages.CreateMsg(tmpMsg.MsgType)
	err = json.Unmarshal(tmpMsg.RawData, msgHelper.DataPointer())

	msg = msgHelper
	return
}

type unparsedMsg struct {
	MsgType  string          `json:"msgType"`
	RawData  json.RawMessage `json:"data"`
	GameId   string          `json:"gameId"`
	GameTick int             `json:"gameTick"`
}
