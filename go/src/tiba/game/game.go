package game

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"tiba/comm"
	"tiba/messages"
)

type GameMode int

const (
	_              = iota
	QUICK GameMode = 1 + iota
	JOIN
	CREATE
)

func StandardFlags() (host string, port int, botId messages.BotId, raceData messages.RaceData, gameMode GameMode) {

	pHost := flag.String("host", "testserver.helloworldopen.com", "Hostname of Testserver")
	pPort := flag.Int("port", 8091, "Port of Testserver")

	// Flags for bot identification
	botName := flag.String("name", "Tiba Noob", "Name of the bot")
	botKey := flag.String("key", "", "Key to authenticate bot")

	// Flags for advanced race creation
	joinRace := flag.Bool("join", false, "Set to true to join an existing race")
	createRace := flag.Bool("create", false, "Set to true to create a race")
	trackName := flag.String("track", "", "Name of the track to join or create")
	password := flag.String("password", "", "Password of the track to join or create")
	carCount := flag.Int("carcount", 1, "Amount of cars in the race")

	flag.Parse()

	if len(*botKey) == 0 {
		log.Fatal("Please provide a key for the bot")
	}

	gameMode = QUICK

	if *createRace && *joinRace {
		log.Fatal("Please only provide either --join or --create, not both")
	}

	if *createRace {
		gameMode = CREATE
	}

	if *joinRace {
		gameMode = JOIN
	}

	if gameMode == CREATE {
		//Check for required arguments
		if len(*trackName) == 0 || len(*password) == 0 {
			log.Fatal("Provide both --track and --password to create a race")
		}
	}

	host = *pHost
	port = *pPort

	botId = messages.BotId{Name: *botName, Key: *botKey}
	raceData = messages.RaceData{CarCount: *carCount, TrackName: *trackName, Password: *password}
	return
}

func StartGame(ml *comm.MsgLoop, botId messages.BotId, raceData messages.RaceData, mode GameMode) (err error) {
	var msg interface{}

	switch mode {
	case QUICK:
		msg, err = messages.CreateJoin(botId)
	case JOIN:
		msg, err = messages.CreateJoinRace(botId, raceData)
	case CREATE:
		msg, err = messages.CreateCreateRace(botId, raceData)
	default:
		err = errors.New("Invalid gamemode")
		return
	}

	if err != nil {
		return
	}

	err = ml.SendMsg(msg)
	if err != nil {
		return
	}

	response, err := ParseMsg(<-ml.ServerOut)
	if err != nil {
		return
	}

	switch m := response.(type) {
	case *messages.CreateRace:
		if msg != *m {
			err = errors.New("Received wrong response from server")
			return
		}
	case *messages.JoinRace:
		if msg != *m {
			err = errors.New("Received wrong response from server")
			return
		}
	case *messages.Join:
		if msg != *m {
			err = errors.New("Received wrong response from server")
			return
		}
	default:
		err = errors.New("Received wrong response from server")
		return

	}

	//now we should receive yourCar
	response, err = ParseMsg(<-ml.ServerOut)
	if err != nil {
		return
	}

	yourCar, ok := response.(*messages.YourCar)

	if !ok {
		//We did not receive your car!
		err = errors.New(fmt.Sprintf("Expected message \"yourCar\", received %T\n", response))
		return
	}

	log.Println("I have color", yourCar.Data.Color)

	SendPing(ml)

	response, err = ParseMsg(<-ml.ServerOut)

	gameInit, ok := response.(*messages.GameInit)
	if !ok {
		err = errors.New(fmt.Sprintf("Expected message \"gameInit\", received %T\n", response))
		return
	}

	log.Println("Received game init, track id is", gameInit.Data.Race.Track.Id)

	for m := range ml.ServerOut {
		var tmp interface{}
		tmp, err = ParseMsg(m)

		if err != nil {
			return
		}

		switch tmp.(type) {
		case *messages.GameStart:
			log.Println("Game is starting!")
			SendPing(ml)
			return
		default:
			log.Println("Received unexpected message:", tmp)
			SendPing(ml)
		}

	}

	return
}

func ParseMsg(payload []byte) (msg interface{}, err error) {
	msg, err = comm.TryParse(payload)

	return
}

func SendPing(ml *comm.MsgLoop) (err error) {
	err = ml.SendMsg(messages.CreatePing(0))
	return
}

func SendThrottle(ml *comm.MsgLoop, throttle float64) (err error) {
	err = ml.SendMsg(messages.CreateThrottle(throttle, 0))
	return
}
