package main

import (
	"errors"
	"fmt"
	"log"
	"math"
	"os"
	"tiba/comm"
	"tiba/game"
	"tiba/messages"
	"time"
)

var throttle float64 = 0.62

func findPosition(positions []messages.CarPosition, color string) (position messages.CarPosition, err error) {
	for _, p := range positions {
		if p.Id.Color == color {
			position = p
			return
		}
	}

	err = errors.New("Failed to find my Position in carPositions")
	return

}

func game_tick(ml *comm.MsgLoop, positions []messages.CarPosition) {
	p, err := findPosition(positions, "red")

	if err != nil {
		log.Println("Failed to find my position: ", err)
	}

	a := math.Abs(float64(p.Angle))
	switch {
	case a < 4:
		throttle += 0.007
	case a > 10:
		throttle -= 0.05
	}

	throttle = math.Min(0.9, throttle)
	throttle = math.Max(0.1, throttle)

	fmt.Println("Angle is", p.Angle)

	fmt.Println("Driving with throttle", throttle)
	game.SendThrottle(ml, throttle)
}

func dispatch_msg(ml *comm.MsgLoop, msg interface{}) (err error) {
	switch m := msg.(type) {
	case *messages.Error:
		err = errors.New(fmt.Sprint("Server-Errormessage: ", m.Data))
		return
	case *messages.CarPositions:
		game_tick(ml, m.Data)
	case *messages.GenericMsg:
		fmt.Println("Received generic message of type", m.MsgType)
		game.SendPing(ml)
	default:
		log.Printf("Got unhandled msg type: %T\n", m)
		game.SendPing(ml)
		return
	}
	return
}

func bot_loop(ml *comm.MsgLoop) (err error) {
	log.Print("Starting loop")
	for {
		msg, ok := <-ml.ServerOut
		if !ok {
			err = errors.New("Server closed connectin")
		}
		input, err := game.ParseMsg(msg)
		if err != nil {
			log_and_exit(err)
		}
		err = dispatch_msg(ml, input)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {
	//log.SetFlags(0) //don't print time in log
	host, port, botId, raceData, gameMode := game.StandardFlags()

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, botId.Name, botId.Key)

	conn, err := comm.Connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	// Start message loop
	fmt.Println("Starting message loop")
	ml, err := comm.StartMessageLoop(conn, time.Second*time.Duration(30))
	if err != nil {
		log.Println("Failed to start message loop")
		log_and_exit(err)
	}

	// Start game
	err = game.StartGame(ml, botId, raceData, gameMode)
	if err != nil {
		log.Print("Failed to start game")
		log_and_exit(err)
	}

	fmt.Println("Sent game start message")

	err = bot_loop(ml)
	if err != nil {
		log.Print("Failure in boot loop")
		log_and_exit(err)
	}
}
